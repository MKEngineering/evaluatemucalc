/* Mucalc.h
 *   return code definitions for Mucalc.c
 *   
 */

#ifndef MUCALC_H
#define MUCALC_H

/* the return codes for Mucalc */
enum {
  no_error = 0,        /* no error */
  no_input,            /* no name and no Z */
  no_zmatch,           /* Z does not match name */
  no_data,             /* data not available for requested material */
  bad_z,               /* bad Z given as input */
  bad_name,            /* invalid element name */
  bad_energy,          /* negative photon energy */
  within_edge,         /* photon energy within 1 eV of an edge */
  m_edge_warn,         /* M-edge data for a Z<30 element requested */
  satan_rules          /* Should never get this error */
};

int Name_to_Z(const char *name);
int MuCalc(const char *name, int ZZ, double ePhoton, char unit, int pFlag,
	  double *energy, double *xSec, double *fluo, char *errmsg);

#endif /* MUCALC_H */
