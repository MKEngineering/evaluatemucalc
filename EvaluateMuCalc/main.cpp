//
// Project:  3D Modeling & Simulation
//
// FileName: main.cpp for EvaluateMuCalc
//
// General Description:  This contains the main program for a program designed to help test the values returned
// by MuCalc() against NIST database values.  A single command-line argument is required and it is the element
// symbol from the periodic table. The output is the mass attenuation coefficient for that material for energies
// ranging from 2keV to 400keV.  The first column is the energy values.  The second column is the mass attenuation
// coefficient in cm^2/g.
//
// Usage:
//     EvaluateMuCalc element
// Examples:
//     EvaluateMuCalc Pb
//
//
// Revision History:  Date    Author
//    Version 0.9  6/20/18    Kupinski
//

#include <iostream>
#include "MuCalc.hh"

int main(int argc, const char * argv[]) {
  if (argc != 2) {
    std::cout << "Usage: EvaluateMuCalc ElementSymbol" << std::endl;
    std::cout << "   where ElementSymbol is from the periodic table of elements." << std::endl;
    exit(0);
  }
  
  const char* element = argv[1];
  
  double energy[9]; // X-ray edge energies returned by mucalc
  double xSec[11];  // cross sections returned by mucalc
  double flYield[4];  // fluorescent yields returned by mucalc
  char errorMessage[256];  // error message returned by mucalc
  
  int N = 5000;
  double minE = 2.0;    // keV
  double maxE = 400.0;  // keV
  
  for (int i = 0;i < N; i++) {
    double photonEnergy = (double)i /(double)(N-1) * (maxE-minE) + minE;
    int err = MuCalc(element, 0, photonEnergy, 'C', 1, energy, xSec, flYield, errorMessage);
    if (err != no_error) {
      std::cout << errorMessage << std::endl;
      exit(0);
    }
    std::cout << photonEnergy << " " << xSec[3]  << std::endl;
  }
  
  
  
  return 0;
}
